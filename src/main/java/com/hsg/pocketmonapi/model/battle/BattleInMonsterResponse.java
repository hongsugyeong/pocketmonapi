package com.hsg.pocketmonapi.model.battle;

import com.hsg.pocketmonapi.model.battle.BattleInMonsterItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BattleInMonsterResponse {

    private BattleInMonsterItem monster1;
    private  BattleInMonsterItem monster2;
}
