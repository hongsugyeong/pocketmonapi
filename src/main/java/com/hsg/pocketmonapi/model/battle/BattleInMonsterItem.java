package com.hsg.pocketmonapi.model.battle;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BattleInMonsterItem {

    private Long monsterBattleId;
    private Long monsterId;
    private String monsterName;
    private Integer monsterVitality;
    private Integer monsterPower;
    private Integer monsterDefensive;
    private Integer monsterSpeed;
}
