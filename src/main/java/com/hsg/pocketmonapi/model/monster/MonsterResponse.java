package com.hsg.pocketmonapi.model.monster;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterResponse {
    private long id;
    private String imgUrl;
    private String charName;
    private String classification;
    private Integer vitality;
    private Integer power;
    private Integer defensive;
    private Integer speed;
}
