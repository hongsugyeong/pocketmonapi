package com.hsg.pocketmonapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "Pocketmon App",
                description = "내 포켓몬은 누구일까",
                version = "vi"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfigure {

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("pocketmon API v1")
                .pathsToMatch(paths)
                .build();
    }
}
