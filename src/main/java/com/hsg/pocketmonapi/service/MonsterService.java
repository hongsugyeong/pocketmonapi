package com.hsg.pocketmonapi.service;

import com.hsg.pocketmonapi.entity.Monster;
import com.hsg.pocketmonapi.model.monster.MonsterCreateRequest;
import com.hsg.pocketmonapi.model.monster.MonsterItem;
import com.hsg.pocketmonapi.model.monster.MonsterResponse;
import com.hsg.pocketmonapi.repository.MonsterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MonsterService {
    private final MonsterRepository monsterRepository;

    public void setMonster (MonsterCreateRequest request) {
        Monster addData = new Monster();

        addData.setImgUrl(request.getImgUrl());
        addData.setCharName(request.getCharName());
        addData.setClassification(request.getClassification());
        addData.setVitality(request.getVitality());
        addData.setPower(request.getPower());
        addData.setDefensive(request.getDefensive());
        addData.setSpeed(request.getSpeed());

        monsterRepository.save(addData);
    }

    public List<MonsterItem> getMonsters () {
        List<Monster> originList = monsterRepository.findAll();

        List<MonsterItem> result = new LinkedList<>();

        for (Monster monster : originList) {
            MonsterItem addList = new MonsterItem();

            addList.setId(monster.getId());
            addList.setImgUrl(monster.getImgUrl());
            addList.setCharName(monster.getCharName());
            addList.setClassification(monster.getClassification());
            addList.setVitality(monster.getVitality());
            addList.setPower(monster.getPower());
            addList.setDefensive(monster.getDefensive());
            addList.setSpeed(monster.getSpeed());

            result.add(addList);
        }

        return result;
    }

    public Monster getData(long id) {
        return monsterRepository.findById(id).orElseThrow();
    }

    public MonsterResponse getMonster (long id) {
        Monster originList = monsterRepository.findById(id).orElseThrow();

        MonsterResponse response = new MonsterResponse();

        response.setId(originList.getId());
        response.setImgUrl(originList.getImgUrl());
        response.setCharName(originList.getCharName());
        response.setClassification(originList.getClassification());
        response.setVitality(originList.getVitality());
        response.setPower(originList.getPower());
        response.setDefensive(originList.getDefensive());
        response.setSpeed(originList.getSpeed());

        return response;
    }
}
