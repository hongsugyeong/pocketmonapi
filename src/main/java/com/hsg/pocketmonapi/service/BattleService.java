package com.hsg.pocketmonapi.service;

import com.hsg.pocketmonapi.entity.Battle;
import com.hsg.pocketmonapi.entity.Monster;
import com.hsg.pocketmonapi.model.battle.BattleInMonsterItem;
import com.hsg.pocketmonapi.model.battle.BattleInMonsterResponse;
import com.hsg.pocketmonapi.repository.BattleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BattleService {
    private final BattleRepository battleRepository;

    // 둘 이상 결투장 진입 불가능하도록 => 몇 마리의 몬스터 있는지 확인
    public void setBattleIn(Monster monster) throws Exception {
        // 결투장에 진입한 몬스터 리스트 가져오기
        List<Battle> checkList = battleRepository.findAll();

        // 만약 checkList 갯수가 2개 이상이면 던지기
        if (checkList.size() >= 2 ) throw new Exception();

        Battle battle = new Battle();
        battle.setMonster(monster);
        battleRepository.save(battle);
    }

    public void delBattleInMonster(long id) {
        battleRepository.deleteById(id);
    }
    // 결투장의 현재 상태

    public BattleInMonsterResponse getCurrentState() {

        // 결투장에 진입한 몬스터 리스트 다 가져오기
        // 2마리 제한 조건에 의해 경우의 수는 0개, 1개, 2개
        List<Battle> checkList = battleRepository.findAll();

        // 모양으로 주기
        BattleInMonsterResponse response = new BattleInMonsterResponse();

        // 몬스터가 2마리 입장했을 경우
        if (checkList.size() == 2) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));
            response.setMonster2(convertMonsterItem(checkList.get(1)));

        }
        // 몬스터가 1마리만 입장했을 경우
        else if (checkList.size() == 1) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));

        }

        return response;
    }

    // 몬스터 정보 가져오기
    private BattleInMonsterItem convertMonsterItem(Battle battle) {
        BattleInMonsterItem monsterItem = new BattleInMonsterItem();
        monsterItem.setMonsterBattleId(battle.getId());
        monsterItem.setMonsterId(battle.getMonster().getId());
        monsterItem.setMonsterName(battle.getMonster().getCharName());
        monsterItem.setMonsterVitality(battle.getMonster().getVitality());
        monsterItem.setMonsterPower(battle.getMonster().getPower());
        monsterItem.setMonsterDefensive(battle.getMonster().getDefensive());
        monsterItem.setMonsterSpeed(battle.getMonster().getSpeed());

        return monsterItem;
    }
}
