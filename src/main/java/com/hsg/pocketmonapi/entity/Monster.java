package com.hsg.pocketmonapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Monster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 20)
    private String imgUrl;

    @Column(nullable = false, length = 20)
    private String charName;

    @Column(nullable = false, length = 20)
    private String classification;

    @Column(nullable = false)
    private Integer vitality;

    @Column(nullable = false)
    private Integer power;

    @Column(nullable = false)
    private Integer defensive;

    @Column(nullable = false)
    private Integer speed;
}
