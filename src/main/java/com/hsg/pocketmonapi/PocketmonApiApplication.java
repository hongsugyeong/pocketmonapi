package com.hsg.pocketmonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocketmonApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocketmonApiApplication.class, args);
    }

}
