package com.hsg.pocketmonapi.repository;

import com.hsg.pocketmonapi.entity.Battle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleRepository extends JpaRepository<Battle, Long> {
}
