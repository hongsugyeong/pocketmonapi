package com.hsg.pocketmonapi.repository;

import com.hsg.pocketmonapi.entity.Monster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonsterRepository extends JpaRepository<Monster, Long> {
}
