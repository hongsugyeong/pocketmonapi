package com.hsg.pocketmonapi.controller;

import com.hsg.pocketmonapi.entity.Monster;
import com.hsg.pocketmonapi.model.CommonResult;
import com.hsg.pocketmonapi.model.SingleResult;
import com.hsg.pocketmonapi.model.battle.BattleInMonsterResponse;
import com.hsg.pocketmonapi.service.BattleService;
import com.hsg.pocketmonapi.service.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/battle")
public class BattleController {
    private final BattleService battleService;
    private final MonsterService monsterService;

    @PostMapping("/stage/in/monster-id/{monsterId}")
    public CommonResult setMonsterStageIn (@PathVariable long monsterId) throws Exception {
        Monster monster = monsterService.getData(monsterId);
        battleService.setBattleIn(monster);

        CommonResult result = new CommonResult();
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }

    @GetMapping("/current/state")
    public SingleResult<BattleInMonsterResponse> getCurrentState () {
        SingleResult<BattleInMonsterResponse> result = new SingleResult<>();
        result.setMsg("성공하였습니다.");
        result.setCode(0);
        result.setData(battleService.getCurrentState());

        return result;
    }

    @DeleteMapping("/stage/out/stage-id/{stageId}")
    public CommonResult delStageOutMonster(@PathVariable long stageId) {
        battleService.delBattleInMonster(stageId);

        CommonResult result = new CommonResult();
        result.setMsg("성공하였습니다.");
        result.setCode(0);

        return result;
    }
}
