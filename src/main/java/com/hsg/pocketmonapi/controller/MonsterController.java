package com.hsg.pocketmonapi.controller;

import com.hsg.pocketmonapi.model.CommonResult;
import com.hsg.pocketmonapi.model.ListResult;
import com.hsg.pocketmonapi.model.SingleResult;
import com.hsg.pocketmonapi.model.monster.MonsterCreateRequest;
import com.hsg.pocketmonapi.model.monster.MonsterItem;
import com.hsg.pocketmonapi.model.monster.MonsterResponse;
import com.hsg.pocketmonapi.service.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/monster")
public class MonsterController {
    private final MonsterService monsterService;

    @PostMapping("/new")
    public CommonResult setMonster(@RequestBody MonsterCreateRequest request) {
        monsterService.setMonster(request);

        CommonResult result = new CommonResult();
        result.setCode(0);
        result.setMsg("등록이 완료되었습니다.");

        return result;
    }

    @GetMapping("/all")
    public ListResult<MonsterItem> getMonsters () {
        List<MonsterItem> list = monsterService.getMonsters();

        ListResult<MonsterItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    @GetMapping("/detail/monster-id/{monsterId}")
    public SingleResult<MonsterResponse> getMonster (@PathVariable long monsterId) {
        MonsterResponse result = monsterService.getMonster(monsterId);

        SingleResult<MonsterResponse> response = new SingleResult<>();
        response.setMsg("성공 축축");
        response.setCode(0);
        response.setData(result);

        return response;
    }

}
